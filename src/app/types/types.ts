export interface User{
    id:string,
    nombre:string,
    email:string,
    perfil:string,
    usuario:string,
    fecha?:Date
  }