import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ServiciosComponent } from './pages/servicios/servicios.component';
import { EmpleadasComponent } from './pages/empleadas/empleadas.component';
import { ProveedoresComponent } from './pages/proveedores/proveedores.component';
import { CiudadesComponent } from './pages/ciudades/ciudades.component';
import { AgendaComponent } from './pages/agenda/agenda.component';
import { ClientesComponent } from './pages/clientes/clientes.component';
import { TarifasComponent } from './pages/tarifas/tarifas.component';
import { PaquetesComponent } from './pages/paquetes/paquetes.component';


const routes: Routes = [
  {
    path: "",
    component: LayoutComponent,
    children: [
      {
        path: "",
        component: DashboardComponent
      },
      {
        path: "servicios",
        component: ServiciosComponent
      },
      {
        path: "empleadas",
        component: EmpleadasComponent
      },
      {
        path: 'proveedores',
        component: ProveedoresComponent
      },
      {
        path: 'ciudades',
        component: CiudadesComponent
      },
      {
        path: 'agenda',
        component: AgendaComponent
      },
      {
        path: 'clientes',
        component: ClientesComponent
      },
      {
        path: 'tarifas',
        component: TarifasComponent
      },
      {
        path: 'paquetes',
        component: PaquetesComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
