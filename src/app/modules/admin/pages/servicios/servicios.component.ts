import { Component, OnInit, TemplateRef } from '@angular/core';
import { MaestrosService } from 'src/app/services/maestros.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-servicios',
  templateUrl: './servicios.component.html',
  styleUrls: ['./servicios.component.css']
})

export class ServiciosComponent implements OnInit {
  services = []
  modalRef: BsModalRef;
  servicioSelected: {} = {}
  current: number = 1;
  total: any;

  constructor(private maestrosService: MaestrosService, private modalService: BsModalService) { }

  ngOnInit(): void {
    this.consultarServicios()
  }

  consultarServicios() {
    this.maestrosService.getServices().subscribe((data: any) => {
      this.services = data.filter(d => d.codigo.trim() !== 'U')
    })
    this.total = this.services.length;
  }

  openModal(modal: TemplateRef<any>) {
    this.servicioSelected = {}
    this.modalRef = this.modalService.show(modal, { class: 'modal-md' })
  }

  openModalUpdate(modal: TemplateRef<any>, servicio) {
    this.servicioSelected = servicio
    this.modalRef = this.modalService.show(modal, { class: 'modal-md' })
  }

  newService(status) {
    if (status) {
      this.modalRef.hide()
      this.consultarServicios()
    }
  }
}
