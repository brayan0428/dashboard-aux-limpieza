import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { UploadService } from 'src/app/services/upload.service';
import Swal from "sweetalert2"
import { MaestrosService } from 'src/app/services/maestros.service';

@Component({
  selector: 'app-nuevo-servicio-form',
  templateUrl: './nuevo-servicio-form.component.html',
  styleUrls: ['./nuevo-servicio-form.component.css']
})
export class NuevoServicioFormComponent implements OnInit {
  @Output() newService = new EventEmitter<boolean>();
  @Input() servicio:{} = {}

  urlImage:string = ''
  habilitado:number = 1;
  loading:boolean = false
  loadingImage:boolean = false
  edit:boolean = false
  constructor(private uploadService:UploadService, private maestrosService:MaestrosService) { }

  ngOnInit(): void {
    console.log(this.servicio)
    this.habilitado = this.servicio["habilitado"] ? this.servicio["habilitado"] : 1
    this.edit = this.servicio["codigo"] ? true : false
  }

  uploadImage(e){
    let type = 'servicios'
    const form = new FormData()
    const file = e.target
    if(file.files.length > 0){
      this.loadingImage = true
      form.append("file", file.files[0])
      form.append("type", type)
      this.uploadService.uploadFile(form, type ).subscribe(data => {
        this.loadingImage = false
        if(data.error){
          Swal.fire('Error', 'Error al subir archivo', 'error')
          return
        }
        this.urlImage = data.url
      }, error => this.loadingImage = false)
    }
  }

  onSubmit(form){
    let data = {...form.value}
    if(this.edit && this.urlImage == ''){
      this.urlImage = this.servicio["icon"]
    }
    data.icon = this.urlImage
    data.codigo = String(data.codigo).toUpperCase()
    if(data.icon == ''){
      Swal.fire('Error', 'Debe cargar el icono', 'warning')
      return
    }
    this.loading = true
    if(this.edit){
      this.maestrosService.updateService(data).subscribe(data => {
        Swal.fire('Confirmación', 'Servicio actualizado exitosamente', 'info')
        this.newService.emit(true)
      }, error => this.loading = false)
    }else{
      this.maestrosService.saveService(data).subscribe(data => {
        Swal.fire('Confirmación', 'Servicio guardado exitosamente', 'info')
        this.newService.emit(true)
      }, error => this.loading = false)
    }
  }
}
