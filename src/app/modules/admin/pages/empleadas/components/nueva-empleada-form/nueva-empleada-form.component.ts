import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { MaestrosService } from 'src/app/services/maestros.service';
import Swal from 'sweetalert2'
import { UploadService } from 'src/app/services/upload.service';
import { onlyNumbers } from '@utils/validations'

@Component({
  selector: 'app-nueva-empleada-form',
  templateUrl: './nueva-empleada-form.component.html',
  styleUrls: ['./nueva-empleada-form.component.css']
})
export class NuevaEmpleadaFormComponent implements OnInit {
  @Input() empleada:any = {}
  @Output() newEmpleada = new EventEmitter<boolean>();
  ciudades: [] = [];
  edit:boolean = false
  loading:boolean = false;
  imageTemp = null
  file = null
  onlyNumbers = onlyNumbers
  constructor(private maestrosService:MaestrosService, private uploadService:UploadService) { }

  ngOnInit(): void {
    this.maestrosService.getCiudades().subscribe((data:any) => {
      this.ciudades = data
    })

    this.edit = this.empleada["codigo"] ? true : false
    if(this.edit){
      this.imageTemp = this.empleada.imagen
    }
  }

  uploadImage(e){
    let type = 'empleadas'
    const form = new FormData()
    const file = e.target
    if(file.files.length > 0){
      this.maestrosService.mostrarCargando()
      form.append("file", file.files[0])
      form.append("type", type)
      this.uploadService.uploadFile(form, type ).subscribe(data => {
        Swal.close()
        if(data.error){
          Swal.fire('Error', 'Error al subir archivo', 'error')
          return
        }
        this.imageTemp = data.url
      })
    }
  }

  onSubmit(form){
    let data = {...form.value}
    data.imagen = this.imageTemp
    this.loading = true
    if(this.edit){
      data.codigo = this.empleada["codigo"]
      this.maestrosService.actualizarEmpleada(data).subscribe(() => {
        this.loading = false
        Swal.fire('Confirmación', 'Empleada actualizada exitosamente', 'success')
        this.newEmpleada.emit(true)
      })
    }else{
      this.maestrosService.saveEmpleada(data).subscribe(() => {
        this.loading = false
        Swal.fire('Confirmación', 'Empleada guardada exitosamente', 'success')
        this.newEmpleada.emit(true)
      })
    }
  }
}
