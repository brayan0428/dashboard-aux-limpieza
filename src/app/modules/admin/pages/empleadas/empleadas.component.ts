import { Component, OnInit } from '@angular/core';
import { MaestrosService } from 'src/app/services/maestros.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-empleadas',
  templateUrl: './empleadas.component.html',
  styleUrls: ['./empleadas.component.css']
})
export class EmpleadasComponent implements OnInit {
  empleadas: [] = []
  empleadasFilter = []
  empleadaSelected: {};
  total: 0;
  current: number = 1;
  modalRef: BsModalRef;

  constructor(private maestrosService: MaestrosService, private modalService: BsModalService) { }

  ngOnInit(): void {
    this.consultarEmpleadas()
  }

  consultarEmpleadas() {
    this.maestrosService.getEmpleadas().subscribe((data: any) => {
      this.empleadas = data
      this.empleadasFilter = data
    })
    this.total = this.empleadas.length;
  }

  openModal(modal) {
    this.empleadaSelected = {}
    this.modalRef = this.modalService.show(modal, { class: 'modal-lg' })
  }

  openModalUpdate(modal, empleada) {
    this.empleadaSelected = empleada
    this.modalRef = this.modalService.show(modal, { class: 'modal-lg' })
  }

  filtrar(e) {
    let { value } = e.target
    value = String(value).toLowerCase()
    if (value === '') {
      this.empleadasFilter = [...this.empleadas]
    } else {
      this.empleadasFilter = this.empleadas.filter(e =>
        String(e["nombre_ciudad"]).toLowerCase().includes(value) ||
        String(e["nombre"]).toLowerCase().includes(value) ||
        String(e["cedula"]).toLowerCase().includes(value) ||
        String(e["codigo"]).toLowerCase().includes(value)
      )
    }
  }

  newEmpleada(e) {
    this.modalRef.hide()
    this.consultarEmpleadas()
  }
}
