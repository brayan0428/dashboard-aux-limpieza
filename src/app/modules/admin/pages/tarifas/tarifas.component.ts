import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { TarifasService } from 'src/app/services/tarifas.service';

@Component({
  selector: 'app-tarifas',
  templateUrl: './tarifas.component.html',
  styleUrls: ['./tarifas.component.css']
})
export class TarifasComponent implements OnInit {
  tarifas = []
  tarifaSelected: any = {}
  modalRef:BsModalRef

  constructor(private tarifasService:TarifasService,private modalService:BsModalService) { }

  ngOnInit(): void {
    this.getTarifas()
  }

  getTarifas(){
    this.tarifasService.getTarifas().subscribe((tarifas:any) => {
      this.tarifas = tarifas
    })
  }

  openModal(modal, tarifa = {}){
    this.tarifaSelected = {...tarifa}
    this.modalRef = this.modalService.show(modal, {class: 'modal-md'})
  }

  saveTarifa(e){
    this.getTarifas()
    this.modalRef.hide()
  }
}
