import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MaestrosService } from 'src/app/services/maestros.service';
import { TarifasService } from 'src/app/services/tarifas.service';
import Swal from 'sweetalert2';
import dataTarifas from '../../../../../../utils/dataTarifas'

@Component({
  selector: 'app-modal-tarifa',
  templateUrl: './modal-tarifa.component.html',
  styleUrls: ['./modal-tarifa.component.css']
})

export class ModalTarifaComponent implements OnInit {
  @Input() tarifa:any = {}
  @Output() saveTarifa = new EventEmitter<boolean>();
  datosTarifa = []
  ciudades:any = []
  servicios:any = []

  constructor(private tarifasService:TarifasService, private maestrosService:MaestrosService) { }

  ngOnInit(): void {
    console.log(this.tarifa)
  
    this.maestrosService.getCiudades().subscribe(ciudades => {
      this.ciudades = ciudades
    })

    this.maestrosService.getServices().subscribe(servicios => {
      this.servicios = servicios
    })

    if(this.tarifa.id){
      this.getTarifa()
    }else{
      this.datosTarifa = [...dataTarifas]
    }
  }

  getTarifa(){
    this.tarifasService.getTarifa(this.tarifa).subscribe((data:any) => {
      this.datosTarifa = data
      this.initialize()
    })
  }

  initialize(){
    this._('ciudad', this.tarifa.codigo_ciudad)
    this._('servicio', this.tarifa.codigo_servicio)
    this._('anio', this.tarifa.anio)
    this._('habilitado', this.datosTarifa[0].habilitado)
  }

  saveData(){
    let data = [...this.datosTarifa]
    data = data.map(d => ({
      ...d,
      codigo_servicio: this._('servicio'),
      anio: this._('anio'),
      codigo_ciudad: this._('ciudad'),
      habilitado: this._('habilitado'),
      id: d.id || ''
    }))
    const validacion = data.find(d => d.valor == '' || parseFloat(d.valor) <= 0 || String(d.valor).length < 5)
    if(validacion){
      Swal.fire('Error', 'Debe ingresar todos los valores', 'error')
      return
    }
    const anio = String(data[0].anio)
    if(anio.length !== 4 || parseInt(anio) <= 0){
      Swal.fire('Error', 'Debe ingresar un año valido', 'error')
      return
    }
    const tarifa = {
      tarifas : [...data]
    }
    this.maestrosService.mostrarCargando()
    console.log(tarifa)
    if(this.tarifa.id){
      this.tarifasService.updateTarifa(tarifa).subscribe((res:any) => {
        Swal.close()
        Swal.fire('Confirmación', res.message, 'success')
        this.saveTarifa.emit(true)
      })
    }else{
      this.tarifasService.saveTarifa(tarifa).subscribe((res:any) => {
        Swal.close()
        Swal.fire('Confirmación', res.message, 'success')
        this.saveTarifa.emit(true)
      })
    }
  }
  
  _(id, value?){
    if(value){
      (<HTMLInputElement>document.getElementById(id)).value = value
      return
    }
    return (<HTMLInputElement>document.getElementById(id)).value
  }
}
