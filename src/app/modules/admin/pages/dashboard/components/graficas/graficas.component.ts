import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-graficas',
  templateUrl: './graficas.component.html',
  styleUrls: ['./graficas.component.css']
})
export class GraficasComponent implements OnInit {

  public colors = ["#109DFA", "#E36B2C", "#02AC66", "#C82A54", "#EF280F", "#8C4966", "#FF689D", "#E69DFB", "#6DC36D", "#222222", "#23BAC4"]

  constructor() { }

  ngOnInit(): void {
    
  }
}
