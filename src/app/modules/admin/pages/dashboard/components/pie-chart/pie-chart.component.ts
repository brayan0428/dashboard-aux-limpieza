import { Component, Input, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';
import { DashboardService } from 'src/app/services/dashboard.service';
import { ExcelService } from 'src/app/services/excel.service';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnInit {
  @Input() tipo:number 
  @Input() colors = [];
  @Input() titulo:string;
  private data:any = []

  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };

  public labelsChart: Label[] = [];
  public dataChart: ChartDataSets[] = [];

  constructor(private dashboardService:DashboardService,
              private excelService:ExcelService) { }

  ngOnInit(): void {
    this.getDataCharts()
  }

  getDataCharts(){
    this.dashboardService.getDataCharts(this.tipo).toPromise().then((data:any) => {
      this.data = data
      const labels = data.map((d:any) => d.variable)
      const dataSet = data.map((d:any) => d.cantidad)
      this.labelsChart = [...labels]
      this.dataChart = [{
        data : [...dataSet],
        backgroundColor: this.colors,
        hoverBackgroundColor: []
      }]
    }).catch(e => console.error(e))
  }

  
  exportarExcel(){
    this.excelService.exportAsExcelFile(this.data, this.titulo)
  }
}
