import { Component, OnInit } from '@angular/core';
import { DashboardService } from 'src/app/services/dashboard.service';
import { ExcelService } from 'src/app/services/excel.service';

@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.css']
})
export class ReportesComponent implements OnInit {
  public reportes:any = []
  public columnas: any = []
  public reporte:any = []
  
  constructor(private dashboardService:DashboardService, private excelService:ExcelService) { }

  ngOnInit(): void {
    this.consultarReporte(0)
  }

  consultarReporte(tipo, filename = ''){
    this.dashboardService.getReportes(tipo).subscribe((data:any) => {
      if(tipo === 0){
        this.reportes = data
      }else{
        if(data.length > 0){
          this.columnas = Object.keys(data[0])
          this.reporte = data
        }
      }
    })
  }

  consultarData(reporte){
    const tipo = reporte.value
    if(tipo == '0') return
    this.consultarReporte(tipo)
  }

  exportar(reporte){
    const filename  = reporte.options[reporte.selectedIndex].text
    console.log(filename)
    this.excelService.exportAsExcelFile(this.reporte, filename)
  }
}
