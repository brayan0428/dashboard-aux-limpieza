import { Component, Input, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';
import { DashboardService } from 'src/app/services/dashboard.service';
import { ExcelService } from 'src/app/services/excel.service';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.css']
})
export class BarChartComponent implements OnInit {
  @Input() tipo:number 
  @Input() colors = [];
  @Input() titulo:string;
  private data:any = []

  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{
      ticks: {
        beginAtZero:true
      }
    }] },
    plugins: {
      datalabels: {
        anchor: "end",
        align: "end",
      },
    },
  };

  public barChartLegend = false;
  public barChartPlugins = [];

  public labelsChart: Label[] = [];
  public dataChart: ChartDataSets[] = [];

  constructor(private dashboardService:DashboardService, private excelService:ExcelService) { }

  ngOnInit(): void {
    this.getDataCharts()
  }

  getDataCharts(){
    this.dashboardService.getDataCharts(this.tipo).toPromise().then((data:any) => {
      this.data = data
      const labels = data.map((d:any) => d.variable)
      const dataSet = data.map((d:any) => d.cantidad)
      this.labelsChart = [...labels]
      this.dataChart = [{
        data : [...dataSet],
        backgroundColor: this.colors,
        hoverBackgroundColor: []
      }]
    }).catch(e => {
      console.error(e)
    })
  }

  exportarExcel(){
    this.excelService.exportAsExcelFile(this.data, this.titulo)
  }
}
