import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MaestrosService } from 'src/app/services/maestros.service';
import { PaquetesService } from 'src/app/services/paquetes.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-paquetes-form',
  templateUrl: './paquetes-form.component.html',
  styleUrls: ['./paquetes-form.component.css']
})
export class PaquetesFormComponent implements OnInit {
  @Input() paquete:any = {}
  @Output() savePaquete = new EventEmitter<boolean>()

  public ciudades: any = []

  constructor(private maestrosService:MaestrosService,
              private paquetesService:PaquetesService) { }

  ngOnInit(): void {
    this.getCiudades()
  }

  getCiudadesPaquete(){
    this.paquetesService.getCiuadesPaquete(this.paquete.id).subscribe((ciudades:any) => {
      ciudades = ciudades.map((c:any) => c.codigo_ciudad)
      this.ciudades = this.ciudades.map((c:any) => (
        {
          ...c, checked: ciudades.includes(c.codigo)
        }
      ))
    })
  }

  getCiudades() {
    this.maestrosService.getCiudades().subscribe((data: any) => {
      this.ciudades = data.filter(d => d.habilitado == '1')
      this.getCiudadesPaquete()
    });
  }

  onSubmit(form){
    let data = {...form.value}
    let ciudades = [];

    document.querySelectorAll('input[type="checkbox"]').forEach((c: any) => {
      if (c.checked) ciudades.push(c.value);
    });
    if (ciudades.length <= 0) {
      Swal.fire(
        "Error",
        "Debe seleccionar las ciudades del paquete",
        "warning"
      );
      return;
    }
    data.ciudades = [...ciudades]
    this.maestrosService.mostrarCargando()
    if(this.paquete.id){
      this.paquetesService.updatePaquete(this.paquete.id,data).subscribe(() => {
        Swal.close()
        Swal.fire('Confirmación', 'Paquete actualizado exitosamente', 'success')
        this.savePaquete.emit(true)
      })
    }else{
      this.paquetesService.savePaquete(data).subscribe(() => {
        Swal.close()
        Swal.fire('Confirmación', 'Paquete guardado exitosamente', 'success')
        this.savePaquete.emit(true)
      })
    }
  }
}
