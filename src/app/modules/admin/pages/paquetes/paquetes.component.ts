import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { PaquetesService } from 'src/app/services/paquetes.service';

@Component({
  selector: 'app-paquetes',
  templateUrl: './paquetes.component.html',
  styleUrls: ['./paquetes.component.css']
})
export class PaquetesComponent implements OnInit {
  public modalRef: BsModalRef;
  public paquetes: any = []
  public paqueteSelected: any;
  current: number = 1;
  total: any;

  constructor(private paquetesService: PaquetesService, private modalService: BsModalService) { }

  ngOnInit(): void {
    this.getPaquetes()
  }

  getPaquetes() {
    this.paquetesService.getPaquetes().subscribe((data: any) => {
      this.paquetes = data
    })
    this.current = this.paquetes.length;
  }

  openModal(modal, p = {}) {
    this.paqueteSelected = { ...p }
    this.modalRef = this.modalService.show(modal, { class: 'modal-md' })
  }

  savePaquete(e) {
    this.modalRef.hide()
    this.getPaquetes()
  }
}
