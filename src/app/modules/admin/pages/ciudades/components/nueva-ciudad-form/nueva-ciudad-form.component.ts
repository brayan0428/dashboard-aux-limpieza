import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import Swal from 'sweetalert2'
import { MaestrosService } from 'src/app/services/maestros.service';

@Component({
  selector: 'app-nueva-ciudad-form',
  templateUrl: './nueva-ciudad-form.component.html',
  styleUrls: ['./nueva-ciudad-form.component.css']
})
export class NuevaCiudadFormComponent implements OnInit {
  @Input() ciudad: any = {}
  @Output() newCiudad = new EventEmitter<boolean>();

  edit:boolean = false;

  constructor(private maestrosService:MaestrosService) { }

  ngOnInit(): void {
    if(this.ciudad.id){
      this.edit = true
    }
  }

  onSubmit(form){
    let data = {...form.value}
    this.maestrosService.mostrarCargando()
    if(this.edit){
      data.id = this.ciudad.id
      console.log(data)
      this.maestrosService.updateCiudad(data).subscribe((data:any) => {
        Swal.close()
        Swal.fire('Confirmación', 'Información guardada exitosamente', 'success')
        this.newCiudad.emit(true)
      })
    }else{
      console.log(data)
      this.maestrosService.saveCiudad(data).subscribe((data:any) => {
        Swal.close()
        this.newCiudad.emit(true)
        Swal.fire('Confirmación', 'Información guardada exitosamente', 'success')
      })
    }
  }
}
