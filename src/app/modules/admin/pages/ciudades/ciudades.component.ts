import { Component, OnInit } from '@angular/core';
import { MaestrosService } from 'src/app/services/maestros.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-ciudades',
  templateUrl: './ciudades.component.html',
  styleUrls: ['./ciudades.component.css']
})
export class CiudadesComponent implements OnInit {
  ciudades: [] = []
  ciudadSelected: {} = {}
  modalRef: BsModalRef
  total: 0;
  p: number = 1;
  constructor(private maestrosService: MaestrosService, private modalService: BsModalService) { }

  ngOnInit(): void {
    this.getCiudades()
  }

  getCiudades() {
    this.maestrosService.getCiudades().subscribe((data: any) => {
      this.ciudades = data
    })
    this.total = this.ciudades.length;
  }

  openModal(modal) {
    this.ciudadSelected = { habilitado: '1' }
    this.modalRef = this.modalService.show(modal, { class: 'modal-sm' })
  }

  openModalUpdate(modal, ciudad) {
    this.ciudadSelected = { ...ciudad }
    this.modalRef = this.modalService.show(modal, { class: 'modal-sm' })
  }

  newCiudad(e) {
    this.getCiudades()
    this.modalRef.hide()
  }
}
