import { Component, OnInit, ViewChild } from "@angular/core";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AgendaService } from 'src/app/services/agenda.service';

@Component({
  selector: "app-agenda",
  templateUrl: "./agenda.component.html",
  styleUrls: ["./agenda.component.css"],
})
export class AgendaComponent implements OnInit {
  events: any[] = [];
  options: any;
  modalRef: BsModalRef;
  idSelected: string = ''
  @ViewChild('modal') modal;
  @ViewChild('modalEditar') modalEditar;

  constructor(private modalService: BsModalService, private agendaService: AgendaService) { }

  ngOnInit(): void {
    this.options = {
      plugins: [dayGridPlugin, timeGridPlugin, interactionPlugin],
      defaultDate: new Date(),
      eventTextColor: "#fff",
      eventBackgroundColor: " blue",


      header: {
        left: "prev,next",
        center: "title",
        right: "dayGridMonth,timeGridWeek,timeGridDay,listWeek",
      },
      editable: true,
      locale: "es",
      eventStartEditable: false,
      eventClick: e => this.eventClick(e),
      dateClick: e => this.dateClick(e)
    };

    this.consultarAgenda()
  }

  consultarAgenda() {
    this.agendaService.getAgenda().subscribe((agenda: any[]) => {
      agenda = agenda.map(a => ({
        id: a.id,
        title: `${a.nombre} (${a.servicio}) `,
        start: a.fecha_servicio,
        end: a.fecha_servicio
      }))
      this.events = [...agenda]
    })
  }

  dateClick(e) {
    this.modalRef = this.modalService.show(this.modal, { class: 'modal-lg' })
  }

  eventClick(e) {
    this.idSelected = e.event.id
    this.modalRef = this.modalService.show(this.modalEditar, { class: 'modal-md' })
  }

  newServicio(e) {
    this.modalRef.hide()
    this.consultarAgenda()
  }

  updateServicio(e) {
    this.consultarAgenda()
    this.modalRef.hide()
  }
}
