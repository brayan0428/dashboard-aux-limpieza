import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AgendaService } from 'src/app/services/agenda.service';
import { MaestrosService } from 'src/app/services/maestros.service';
import Swal from 'sweetalert2';
import * as moment from 'moment'

@Component({
  selector: 'app-editar-evento-form',
  templateUrl: './editar-evento-form.component.html',
  styleUrls: ['./editar-evento-form.component.css']
})
export class EditarEventoFormComponent implements OnInit {
  empleadas:any = []
  ciudades:any = []
  servicios:any = []
  servicio:any = {}
  @Input() id:string = ''
  @Output() updateServicio = new EventEmitter<boolean>()

  constructor(private maestrosService:MaestrosService, private agendaService:AgendaService) { }

  ngOnInit(): void {
    this.agendaService.getServicio(this.id).subscribe(data => {
      this.servicio = data[0]
      this.consultarEmpleadas()
    })

    this.maestrosService.getCiudades().subscribe(data => {
      this.ciudades = data
    })

    this.maestrosService.getServices().subscribe(data => {
      this.servicios = data
    })
  }

  consultarEmpleadas(){
    this.maestrosService.getEmpleadas().subscribe((data: []) => {
      this.empleadas = data.filter((d:any) => d.habilitado == '1' && d.ciudad == this.servicio.ciudad)
    })
  }

  onSubmit(form){
    const data = {...form.value}
    data.hora_fin = moment(`${data.hora_inicio}:00`,'hh:mm').add(this.servicio.horas,'hours').format('HH:mm')
    this.maestrosService.mostrarCargando()
    this.agendaService.updateServicio(this.id, data).subscribe(() => {
      Swal.close()
      Swal.fire('Confirmación', 'Servicio actualizado exitosamente', 'success')
      this.updateServicio.emit(true)
    })
  }

  eliminarServicio(){
    Swal.fire({
      title: 'Anular servicio',
      text: "¿Está seguro que desea anular el servicio?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si'
    }).then((result) => {
      if (result.isConfirmed) {
        this.maestrosService.mostrarCargando()
        this.agendaService.deleteServicio(this.id).subscribe(() => {
          Swal.close()
          Swal.fire('Confirmación', 'Servicio anulado', 'success')
          this.updateServicio.emit(true)
        })
      }
    })
  }
}
