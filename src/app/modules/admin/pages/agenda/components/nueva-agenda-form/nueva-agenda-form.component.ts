import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { MaestrosService } from "src/app/services/maestros.service";
import { AgendaService } from "src/app/services/agenda.service";
import Swal from 'sweetalert2';
import * as moment from "moment";
import { onlyNumbers } from '@utils/validations'

@Component({
  selector: "app-nueva-agenda-form",
  templateUrl: "./nueva-agenda-form.component.html",
  styleUrls: ["./nueva-agenda-form.component.css"],
})
export class NuevaAgendaFormComponent implements OnInit {
  onlyNumbers = onlyNumbers
  @Output() newServicio = new EventEmitter<boolean>();
  empleadas: any[] = [];
  servicios: any[] = [];
  clientes:any[] = []
  asociados:any[] = []
  ciudades:any[] = []
  formaspago:any[] = []
  msgError:string = ""
  valor_servicio:number = 0
  info_asociado = {
    direccion: '',
    ciudad: ''
  }
  usuarioSelected = ''

  constructor(
    private maestrosService: MaestrosService,
    private agendaService: AgendaService
  ) {}

  ngOnInit(): void {
    this.maestrosService.getServices().subscribe((servicios: any[]) => {
      this.servicios = servicios.filter((s) => s.habilitado == "1");
    });

    this.agendaService.getFormasPago().subscribe((data:any) => {
      this.formaspago = data
    })    

    this.consultarClientes()
    this.consultarCiudades()
  }

  consultarEmpleadas(){
    this.maestrosService.getEmpleadas().subscribe((empleadas: any[]) => {
      this.empleadas = empleadas.filter((e) => e.habilitado == "1" && e.ciudad == this.info_asociado.ciudad);
    });
  }
  
  consultarClientes() {
    this.agendaService
      .getClientes()
      .toPromise()
      .then((clientes: any[]) => {
        this.clientes = clientes
      });
  }

  consultarClientesAsociados(e){
    if(e){
      this.agendaService.getClientesAsociados(e.id).toPromise().then((asociados:any[]) => {
        this.asociados = asociados.filter(a => a.habilitado == '1')
        if(this.asociados.length === 1){
          this.usuarioSelected = this.asociados[0].id
          this.consultarInfo(this.usuarioSelected)
        }
      })
    }else{
      this.asociados = []
    }
  }

  consultarCiudades(){
    this.maestrosService.getCiudades().toPromise().then((ciudades:any[]) => {
      this.ciudades = ciudades.filter(c => c.habilitado == '1')
    })
  }

  cambiarInfo(e){
    const {value} = e.target
    this.consultarInfo(value)
  }

  consultarInfo(value){
    if(!value){
      this.info_asociado = {
        direccion: '',
        ciudad: ''
      }
      this.empleadas = []
      return
    }
    const asociado = this.asociados.find(a => a.id == value)
    this.info_asociado = {...this.info_asociado, direccion: asociado.direccion, ciudad: asociado.ciudad}
    this.consultarEmpleadas()
  }

  consultarValor(form){
    const data = form.value
    if(data.dias > 0 && data.horas > 0){
      this.agendaService.getValorServicio(data).subscribe((data:any) => {
        data = JSON.parse(data)[0]
        if(parseInt(data.Dias_Especiales) > 0 &&  parseInt(data.Dias_Especiales) !== parseInt(data.Dias)){
          this.msgError = "Los servicios para domingos y festivos deben ser unitarios"
          Swal.fire('Error', this.msgError, 'warning')
          return
        }
        if(parseInt(data.Dias_Especiales) > 0 && parseInt(data.Horas) < 4){
          this.msgError = "Los domingos y festivos el servicio minimo es de 4 horas"
          Swal.fire('Error', this.msgError, 'warning')
          return
        }
        this.valor_servicio = data.Total
      })
    }
  }

  onSubmit(form){
    if(this.msgError !== ""){
      Swal.fire('Error', this.msgError, 'warning')
      return
    }
    const data = form.value
    const hora_inicio = data.hora_inicio
    data.hora_fin = moment(`${hora_inicio}:00`,'hh:mm').add(data.horas,'hours').format('HH:mm')
    if(!this.validTime(data.hora_inicio) || !this.validTime(data.hora_fin)){
      Swal.fire('Error', 'El horario de agendamiento debe ser entre las 7:00 AM y las 6:00 PM','error')
      return
    }
    data.estado = 2
    this.maestrosService.mostrarCargando()
    this.agendaService.saveServicio(data).subscribe(() => {
      Swal.close()
      this.newServicio.emit(true)
      Swal.fire('Confirmación', 'Servicio agendado exitosamente','success')
    })
  }

  validTime(time){
    const format = 'HH:mm'
    const t = moment(time, format)
    let beforeTime = moment('06:59', format),
        afterTime = moment('18:01', format)
    return t.isBetween(beforeTime,afterTime)
  }
}
