import { Component, OnInit } from "@angular/core";
import { UsuariosService } from "src/app/services/usuarios.service";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";


@Component({
  selector: "app-clientes",
  templateUrl: "./clientes.component.html",
  styleUrls: ["./clientes.component.css"],
})
export class ClientesComponent implements OnInit {
  clientes: [] = [];
  clientesFilter: [] = [];
  clienteSelected: {} = {};
  idCliente: string = '';
  p: number = 1;
  total: any;
  modalRef: BsModalRef;
  constructor(
    private usuariosService: UsuariosService,
    private modalService: BsModalService
  ) { }

  ngOnInit(): void {
    this.getUsuarios();
  }

  getUsuarios() {
    this.usuariosService
      .getUsuarios()
      .toPromise()
      .then((data: []) => {
        this.clientes = data;
        this.clientesFilter = data;
      });
    this.total = this.clientes.length;
  }

  filtrar(e) {
    let { value } = e.target;
    value = String(value).toLowerCase();
    if (value === "") {
      this.clientesFilter = <[]>[...this.clientes];
    } else {
      this.clientesFilter = <[]>(
        this.clientes.filter(
          (e: any) =>
            String(e.nombre_ciudad).toLowerCase().includes(value) ||
            String(e.nombre).toLowerCase().includes(value) ||
            String(e.cedula).toLowerCase().includes(value)
        )
      );
    }
  }

  openModal(modal, cliente = { habilitado: 1 }) {
    this.clienteSelected = cliente;
    this.modalRef = this.modalService.show(modal, { class: "modal-lg" });
  }

  newCliente(e) {
    this.modalRef.hide();
    this.getUsuarios();
  }

  openModalAsociados(modal, idCliente) {
    this.idCliente = idCliente
    this.modalRef = this.modalService.show(modal, { class: "modal-lg" })
  }
}
