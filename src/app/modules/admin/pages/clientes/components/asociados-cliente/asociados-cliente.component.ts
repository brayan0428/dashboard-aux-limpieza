import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { MaestrosService } from 'src/app/services/maestros.service';
import { UsuariosService } from 'src/app/services/usuarios.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-asociados-cliente',
  templateUrl: './asociados-cliente.component.html',
  styleUrls: ['./asociados-cliente.component.css']
})
export class AsociadosClienteComponent implements OnInit {
  @Input() idCliente:string;
  asociados: any = []
  selectedUser:any = {}
  selectedTab:number = 1
  ciudades:any = []
  @ViewChild('staticTabs', { static: false }) staticTabs: TabsetComponent;

  constructor(private usuariosService:UsuariosService,
              private maestrosService:MaestrosService) { }

  ngOnInit(): void {
    this.consultarUsuarios()
    this.maestrosService.getCiudades().subscribe(data => {
      this.ciudades = data
    })
  }

  consultarUsuarios(){
    this.usuariosService.getClientesAsociados(this.idCliente).subscribe((data:[]) => {
      this.asociados = data.filter((d:any) => d.usuario_principal == '0')
    })
  }

  editarUsuario(usuario){
    this.selectedUser = {...usuario}
    this.changeTab(1)
  }

  changeTab(i){
    this.staticTabs.tabs[i].active = true
  }

  cancelar(){
    this.selectedUser = {habilitado: 1, ciudad: ''}
    this.changeTab(0)
  }

  onSubmit(form){
    const data = {...form.value}
    data.id_usuario = this.idCliente
    data.id = this.selectedUser.nombre ? this.selectedUser.id : 0
    this.maestrosService.mostrarCargando()
    this.usuariosService.saveClientesAsociados(data).subscribe(async () => {
      Swal.close()
      await Swal.fire('Confirmación', 'Información guardada exitosamente', 'success')
      this.consultarUsuarios()  
      this.cancelar()
    })
  }
}
