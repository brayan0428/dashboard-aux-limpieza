import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MaestrosService } from 'src/app/services/maestros.service';
import { UsuariosService } from 'src/app/services/usuarios.service';
import Swal from 'sweetalert2';
import { onlyNumbers } from '@utils/validations'

@Component({
  selector: 'app-nuevo-cliente-form',
  templateUrl: './nuevo-cliente-form.component.html',
  styleUrls: ['./nuevo-cliente-form.component.css']
})
export class NuevoClienteFormComponent implements OnInit {
  @Input() cliente:any = {}
  @Output() newCliente = new EventEmitter<boolean>()
  onlyNumbers = onlyNumbers
  ciudades:[] = []
  constructor(private maestrosService:MaestrosService,
              private usuariosService:UsuariosService) { }

  ngOnInit(): void {
    this.consultarCiudades()
  }

  consultarCiudades(){
    this.maestrosService.getCiudades().toPromise()
      .then((ciudades:[]) => {
        this.ciudades = <[]>ciudades.filter((c:any) => c.habilitado == "1")
      })
  }

  onSubmit(form){
    const data = {...form.value}
    this.maestrosService.mostrarCargando()
    console.log(data.cedula.length)
    if(String(data.cedula).length < 7){
      Swal.fire('Error', 'La cedula debe tener minimo 7 caracteres', 'error')
      return
    }
    if(this.cliente.id){
      data.id = this.cliente.id
      this.usuariosService.updateUsuario(data).subscribe(() => {
        Swal.close()
        this.newCliente.emit(true)
        Swal.fire('Confirmación', 'Información guardada exitosamente', 'success')
      })
    }else{
      data.clave = data.cedula
      data.perfil = 1
      this.usuariosService.saveUsuario(data).subscribe(() => {
        Swal.close()
        this.newCliente.emit(true)
        Swal.fire('Confirmación', 'Información guardada exitosamente', 'success')
      })
    }
  }
}
