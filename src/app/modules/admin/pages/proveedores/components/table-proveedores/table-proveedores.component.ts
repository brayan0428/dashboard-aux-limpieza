import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-table-proveedores',
  templateUrl: './table-proveedores.component.html',
  styleUrls: ['./table-proveedores.component.css']
})
export class TableProveedoresComponent implements OnInit {
  p: number = 1;
  total: any;
  @Input() proveedores: [] = []
  @Output() openModal = new EventEmitter<any>()

  constructor() { }

  ngOnInit(): void {
  }

  abrirModal(proveedor) {
    this.openModal.emit(proveedor)
  }
}
