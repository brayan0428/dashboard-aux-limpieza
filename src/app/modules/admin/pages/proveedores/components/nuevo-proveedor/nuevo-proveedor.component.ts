import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import { MaestrosService } from "src/app/services/maestros.service";
import Swal from "sweetalert2";
import { onlyNumbers } from '@utils/validations'

@Component({
  selector: "app-nuevo-proveedor",
  templateUrl: "./nuevo-proveedor.component.html",
  styleUrls: ["./nuevo-proveedor.component.css"],
})
export class NuevoProveedorComponent implements OnInit {
  @Input() proveedor: any = {};
  @Output() newProveedor = new EventEmitter<boolean>();
  servicios: [] = [];
  grupos: [] = [];
  ciudades: [] = [];
  tipodoc: [] = [];
  actualizar:boolean = false
  grupo: string = "";
  onlyNumbers = onlyNumbers
  constructor(private maestrosService: MaestrosService) {}

  ngOnInit(): void {
    this.getGrupos();
    this.getCiudades();
    this.getTipoDoc();
    console.log(this.proveedor);
    if (Object.keys(this.proveedor).length > 0) {
      this.getEspecialidadesProveedor(this.proveedor.id);
      this.actualizar = true
    }
  }

  getGrupos() {
    this.maestrosService.getGrupos().subscribe((data: any) => {
      this.grupos = data;
    });
  }

  getEspecialidades(servicios = []) {
    this.maestrosService
      .getEspecialidades(this.grupo)
      .subscribe((data:[]) => {
        this.servicios = <any>data.map((d:any) => {
          return {...d, checked:servicios.includes(d.codigo) }
        })
      });
  }

  getCiudades() {
    this.maestrosService.getCiudades().subscribe((data: any) => {
      this.ciudades = data;
    });
  }

  getTipoDoc() {
    this.maestrosService.getTipoDoc().subscribe((data: any) => {
      this.tipodoc = data;
    });
  }

  onSubmit(form) {
    console.log(form.value);
    let especialidades = [];

    document.querySelectorAll('input[type="checkbox"]').forEach((c: any) => {
      if (c.checked) especialidades.push(c.value);
    });
    if (especialidades.length <= 0) {
      Swal.fire(
        "Error",
        "Debe seleccionar los servicios del proveedor",
        "warning"
      );
      return;
    }
    this.mostrarCargando();
    let data = { ...form.value, especialidades };
    if(this.actualizar){
      data = {...data, id: this.proveedor.id}
      this.maestrosService.updateProveedor(data).subscribe((data) => {
        Swal.close();
        Swal.fire("Confirmación", "Proveedor actualizado exitosamente", "success");
        this.newProveedor.emit(true);
      });
    }else{
      this.maestrosService.saveProveedor(data).subscribe((data) => {
        Swal.close();
        Swal.fire("Confirmación", "Proveedor guardado exitosamente", "success");
        this.newProveedor.emit(true);
      });
    }
  }

  mostrarCargando() {
    Swal.fire({
      title: "Cargando...",
      allowOutsideClick: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
  }

  getEspecialidadesProveedor(id) {
    this.maestrosService
      .getEspecialidadesProveedor(id)
      .subscribe((data: any) => {
        console.log(data);
        this.grupo = data[0].codigo_grupo;
        const servicios = data.reduce((a, b) => {
          return [...a, b.id_especialidad];
        }, []);
        this.getEspecialidades(servicios)
      });
  }
}
