import { Component, OnInit } from "@angular/core";
import { MaestrosService } from "src/app/services/maestros.service";
import Swal from 'sweetalert2'
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: "app-proveedores",
  templateUrl: "./proveedores.component.html",
  styleUrls: ["./proveedores.component.css"],
})
export class ProveedoresComponent implements OnInit {
  grupos = [];
  especialidades: [] = [];
  proveedores: [] = []
  especialidad = "";
  modalRef: BsModalRef;
  proveedorSelected: {} = {}
  p: number = 1;
  total: any;

  constructor(private maestrosService: MaestrosService, private modalService: BsModalService) { }

  ngOnInit(): void {
    this.getGrupos();
  }

  getGrupos() {
    this.maestrosService.getGrupos().subscribe((data: any) => {
      this.grupos = data;
    });
  }

  getEspecialidades(e) {
    this.especialidades = [];
    const grupo = e.target.value;
    this.maestrosService.getEspecialidades(grupo).subscribe((data: any) => {
      this.especialidades = data;
    });
  }

  getProveedores() {
    if (!this.especialidad) {
      Swal.fire('Error', 'Debe seleccionar la especialidad', 'warning')
      return
    }
    this.maestrosService
      .getProveedores(this.especialidad)
      .subscribe((data: any) => {
        this.proveedores = data
      });
  }

  openModal(modal, proveedor) {
    this.proveedorSelected = proveedor
    this.modalRef = this.modalService.show(modal, { class: 'modal-lg' })
  }

  newProveedor(value) {
    this.modalRef.hide()
  }
}
