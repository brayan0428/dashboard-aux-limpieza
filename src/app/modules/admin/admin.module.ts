import { NgModule } from "@angular/core";
import { NgxPaginationModule } from 'ngx-pagination';
import { CommonModule } from "@angular/common";
import { ModalModule } from "ngx-bootstrap/modal";
import { AdminRoutingModule } from "./admin-routing.module";
import { DashboardComponent } from "./pages/dashboard/dashboard.component";
import { LayoutComponent } from "./layout/layout.component";
import {
  AppHeaderModule,
  AppSidebarModule,
  AppFooterModule,
} from "@coreui/angular";
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { ChartsModule } from "ng2-charts";
import { FormsModule } from "@angular/forms";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { ServiciosComponent } from "./pages/servicios/servicios.component";
import { ServicesModule } from "src/app/services/services.module";
import { ModalComponent } from "./components/modal/modal.component";
import { NuevoServicioFormComponent } from "./pages/servicios/components/nuevo-servicio-form/nuevo-servicio-form.component";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { InterceptorService } from "src/app/services/interceptor.service";
import { EmpleadasComponent } from "./pages/empleadas/empleadas.component";
import { NuevaEmpleadaFormComponent } from "./pages/empleadas/components/nueva-empleada-form/nueva-empleada-form.component";
import { ProveedoresComponent } from "./pages/proveedores/proveedores.component";
import { TableProveedoresComponent } from "./pages/proveedores/components/table-proveedores/table-proveedores.component";
import { NuevoProveedorComponent } from "./pages/proveedores/components/nuevo-proveedor/nuevo-proveedor.component";
import { CiudadesComponent } from "./pages/ciudades/ciudades.component";
import { NuevaCiudadFormComponent } from "./pages/ciudades/components/nueva-ciudad-form/nueva-ciudad-form.component";
import { AgendaComponent } from "./pages/agenda/agenda.component";
import { FullCalendarModule } from "primeng/fullcalendar";
import { NuevaAgendaFormComponent } from "./pages/agenda/components/nueva-agenda-form/nueva-agenda-form.component";
import { NgSelectModule } from "@ng-select/ng-select";
import { ClientesComponent } from "./pages/clientes/clientes.component";
import { NuevoClienteFormComponent } from "./pages/clientes/components/nuevo-cliente-form/nuevo-cliente-form.component";
import { AsociadosClienteComponent } from "./pages/clientes/components/asociados-cliente/asociados-cliente.component";
import { TabsModule } from "ngx-bootstrap/tabs";
import { TarifasComponent } from "./pages/tarifas/tarifas.component";
import { ModalTarifaComponent } from "./pages/tarifas/components/modal-tarifa/modal-tarifa.component";
import { EditarEventoFormComponent } from "./pages/agenda/components/editar-evento-form/editar-evento-form.component";
import { GraficasComponent } from './pages/dashboard/components/graficas/graficas.component';
import { BarChartComponent } from './pages/dashboard/components/bar-chart/bar-chart.component';
import { PieChartComponent } from './pages/dashboard/components/pie-chart/pie-chart.component';
import { ReportesComponent } from './pages/dashboard/components/reportes/reportes.component';
import { PaquetesComponent } from './pages/paquetes/paquetes.component';
import { PaquetesFormComponent } from './pages/paquetes/components/paquetes-form/paquetes-form.component';

@NgModule({
  declarations: [
    DashboardComponent,
    LayoutComponent,
    ServiciosComponent,
    ModalComponent,
    NuevoServicioFormComponent,
    EmpleadasComponent,
    NuevaEmpleadaFormComponent,
    ProveedoresComponent,
    TableProveedoresComponent,
    NuevoProveedorComponent,
    CiudadesComponent,
    NuevaCiudadFormComponent,
    AgendaComponent,
    NuevaAgendaFormComponent,
    ClientesComponent,
    NuevoClienteFormComponent,
    AsociadosClienteComponent,
    EditarEventoFormComponent,
    TarifasComponent,
    ModalTarifaComponent,
    GraficasComponent,
    BarChartComponent,
    PieChartComponent,
    ReportesComponent,
    PaquetesComponent,
    PaquetesFormComponent
  ],
  imports: [
    CommonModule,
    NgxPaginationModule,
    AdminRoutingModule,
    AppHeaderModule,
    AppSidebarModule,
    AppFooterModule,
    PerfectScrollbarModule,
    ChartsModule,
    FormsModule,
    BsDropdownModule.forRoot(),
    ServicesModule,
    ModalModule.forRoot(),
    FullCalendarModule,
    NgSelectModule,
    TabsModule.forRoot()
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true,
    },
  ],
})
export class AdminModule { }
