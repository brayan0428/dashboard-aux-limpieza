import { Component, OnInit, ViewChild, Input} from '@angular/core';
import { ModalDirective, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  @Input() modalRef:BsModalRef;
  @Input() title:String;
  
  constructor() { }

  ngOnInit(): void {
  }
}
