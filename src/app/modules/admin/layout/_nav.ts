import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Dashboard',
    url: '/admin',
    icon: 'icon-graph',
  },
  {
    name: 'Ciudades',
    url: '/admin/ciudades',
    icon: 'icon-flag'
  },
  {
    name: 'Servicios',
    url: '/admin/servicios',
    icon: 'icon-list'
  },
  {
    name: 'Empleadas',
    url: '/admin/empleadas',
    icon: 'icon-people'
  },
  {
    name: 'Proveedores',
    url: '/admin/proveedores',
    icon: 'icon-people'
  },
  {
    name: 'Clientes',
    url: '/admin/clientes',
    icon: 'icon-organization'
  },
  {
    name: 'Agenda',
    url: '/admin/agenda',
    icon: 'icon-calendar'
  },
  {
    name: 'Tarifas',
    url: '/admin/tarifas',
    icon: 'icon-info'
  },
  {
    name: 'Paquetes',
    url: '/admin/paquetes',
    icon: 'icon-present'
  },
 ];
