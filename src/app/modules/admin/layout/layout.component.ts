import { Component, OnInit } from '@angular/core';
import { navItems } from './_nav';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { User } from 'src/app/types/types';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styles: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {
  public sidebarMinimized = false;
  public navItems = navItems;
  user: User;

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.user = this.authService.getUser()
  }

  logout(e) {
    e.preventDefault()
    this.authService.removeUser()
    this.router.navigate(['/'])
  }
}
