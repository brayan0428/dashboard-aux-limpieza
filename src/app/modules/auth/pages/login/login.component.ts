import { Component, OnInit } from '@angular/core';
import { sha256 } from 'js-sha256'
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService:AuthService, private _router:Router) { }

  ngOnInit(): void {
  }

  onSubmit(form){
    const data = {...form.value}
    data.clave = sha256(data.clave)
    this.authService.login(data).subscribe(() => this._router.navigate(["/admin"]) )
  }
}
