export function onlyNumbers(e){
    const element = e.target
    const num = element.value.match(/^\d+$/);
    if (num === null) {
        element.value = ''
    }
}