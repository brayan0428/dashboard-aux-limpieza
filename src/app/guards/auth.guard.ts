import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  CanActivateChild,
  Router,
} from "@angular/router";
import { Observable } from "rxjs";
import { AuthService } from "../services/auth.service";
import { differenceInHours } from "date-fns";

@Injectable({
  providedIn: "root",
})
export class AuthGuard implements CanActivateChild {
  constructor(private userService: AuthService, private router: Router) {}

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    const user = this.userService.getUser();
    const url = state.url;
    if (user) {
      const hours = differenceInHours(new Date(), new Date(user.fecha));
      if (hours > 12) {
        this.userService.removeUser();
        this.router.navigate(["/"]);
        return false;
      }
      if(url === '/'){
        this.router.navigate(['/admin'])
        return false
      }
    } else {
      if (url !== "/") {
        this.router.navigate(["/"]);
        return false;
      }
    }
    return true;
  }
}
