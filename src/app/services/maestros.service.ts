import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2'

@Injectable({
  providedIn: 'root'
})
export class MaestrosService {

  constructor(private _http:HttpClient) { }
  
  mostrarCargando() {
    Swal.fire({
      title: "Cargando...",
      allowOutsideClick: false,
      onBeforeOpen: () => {
        Swal.showLoading();
      },
    });
  }

  getCiudades(){
    return this._http.get(`${environment.URL_API}/admin/ciudades`);
  }

  getServices(todos = false){
    return this._http.get(`${environment.URL_API}/admin/servicios`)
  }

  saveService(data){
    return this._http.post(`${environment.URL_API}/admin/servicio`, data)
  }

  updateService(data){
    return this._http.put(`${environment.URL_API}/admin/servicio`, data)
  }

  getEmpleadas(){
    return this._http.get(`${environment.URL_API}/admin/empleadas`);
  }

  saveEmpleada(data){
    return this._http.post(`${environment.URL_API}/admin/empleada`, data)
  }

  actualizarEmpleada(data){
    return this._http.put(`${environment.URL_API}/admin/empleada`, data)
  }

  getGrupos(){
    return this._http.get(`${environment.URL_API}/admin/grupos`);
  }

  getEspecialidades(grupo){
    return this._http.get(`${environment.URL_API}/admin/especialidades/${grupo}`);
  }

  getProveedores(id){
    return this._http.get(`${environment.URL_API}/admin/proveedores/${id}`);
  }

  getTipoDoc(){
    return this._http.get(`${environment.URL_API}/admin/tipodoc`);
  }

  
  saveProveedor(data){
    return this._http.post(`${environment.URL_API}/admin/proveedor`, data)
  }

  updateProveedor(data){
    return this._http.put(`${environment.URL_API}/admin/proveedor`, data)
  }

  getEspecialidadesProveedor(id){
    return this._http.get(`${environment.URL_API}/admin/especialidades-proveedor/${id}`);
  }

  saveCiudad(data){
    return this._http.post(`${environment.URL_API}/admin/ciudades`, data)
  }

  updateCiudad(data){
    return this._http.put(`${environment.URL_API}/admin/ciudades`, data)
  }
}
