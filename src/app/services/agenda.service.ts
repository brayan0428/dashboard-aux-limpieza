import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AgendaService {

  constructor(private _http:HttpClient) { }

  getClientes(){
    return this._http.get(`${environment.URL_API}/admin/clientes`)
  }

  getClientesAsociados(id){
    return this._http.get(`${environment.URL_API}/admin/clientes-asociados/${id}`)
  }

  getValorServicio(data){
    return this._http.post(`${environment.URL_API}/valorservicio`, data)
  }

  getFormasPago(){
    return this._http.get(`${environment.URL_API}/admin/formaspago`)
  }

  saveServicio(data){
    return this._http.post(`${environment.URL_API}/admin/servicios`, data)
  }

  getAgenda(){
    return this._http.get(`${environment.URL_API}/admin/agenda`)
  }

  getServicio(id){
    return this._http.get(`${environment.URL_API}/admin/servicio/${id}`)
  }

  updateServicio(id, data){
    return this._http.put(`${environment.URL_API}/admin/servicio/${id}`, data)
  }

  deleteServicio(id){
    return this._http.delete(`${environment.URL_API}/admin/servicio/${id}`)
  }
}
