import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import * as CryptoJS from 'crypto-js'
import { map } from 'rxjs/operators';
import { User } from '../types/types';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private _http:HttpClient) { }

  saveUser(user){
    const encryptedUser = CryptoJS.AES.encrypt(JSON.stringify(user), environment.KEY_AUTH_USER)
    localStorage.setItem('user', encryptedUser.toString())
  }

  getUser():User{
    const user = localStorage.getItem('user')
    if(!user){
      return null
    }
    const decrypt = CryptoJS.AES.decrypt(user, environment.KEY_AUTH_USER)
    const decryptUser = decrypt.toString(CryptoJS.enc.Utf8)
    return JSON.parse(decryptUser)[0]
  }

  login(data){
    return this._http.post(`${environment.URL_API}/auth/login`, data).pipe(map((data:User) => {
      data[0].fecha = new Date()
      this.saveUser(data)
      return data
    }))
  }

  removeUser(){
    localStorage.removeItem('user')
  }
}
