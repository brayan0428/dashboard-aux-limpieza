import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PaquetesService {

  constructor(private http:HttpClient) { }

  getPaquetes(){
    return this.http.get(`${environment.URL_API}/admin/paquetes`)
  }

  getCiuadesPaquete(id){
    return this.http.get(`${environment.URL_API}/admin/ciudades-paquete/${id}`)
  }

  savePaquete(paquete){
    return this.http.post(`${environment.URL_API}/admin/paquete`, paquete)
  }

  updatePaquete(id,paquete){
    return this.http.put(`${environment.URL_API}/admin/paquete/${id}`, paquete)
  }
}
