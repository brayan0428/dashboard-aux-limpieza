import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor(private _http:HttpClient) { }

  uploadFile(data, type){
    return this._http.post(`${environment.URL_API}/admin/upload`, data).pipe(map((data:any) => {
      if(!data.error){
        let url = `${environment.URL_FILES}/${type}/${data.message}`
        data.url = url
        return data
      }
      return data
    }))
  }
}
