import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http:HttpClient) { }

  getDataCharts(tipo){
    return this.http.get(`${environment.URL_API}/admin/charts/${tipo}`)
  }

  getReportes(tipo){
    return this.http.get(`${environment.URL_API}/admin/reportes/${tipo}`)
  }
}
