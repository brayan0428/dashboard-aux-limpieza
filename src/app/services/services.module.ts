import { NgModule } from '@angular/core';
import { MaestrosService } from './maestros.service';
import { UploadService } from './upload.service';
import { AuthService } from './auth.service';

@NgModule({
    imports: [
    ],
    providers: [
        MaestrosService,
        UploadService,
        AuthService
    ],
})

export class ServicesModule { }
