import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  constructor(private http:HttpClient) { }

  getUsuarios(){
    return this.http.get(`${environment.URL_API}/admin/usuarios`)
  }

  saveUsuario(data){
    return this.http.post(`${environment.URL_API}/admin/usuarios`, data)
  }

  updateUsuario(data){
    return this.http.put(`${environment.URL_API}/admin/usuarios`, data)
  }

  getClientesAsociados(id){
    return this.http.get(`${environment.URL_API}/admin/clientes-asociados/${id}`)
  }

  saveClientesAsociados(data){
    return this.http.post(`${environment.URL_API}/admin/cliente-asociado`, data)
  }
}
