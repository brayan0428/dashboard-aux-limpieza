import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TarifasService {

  constructor(private http:HttpClient) { }

  getTarifas(){
    return this.http.get(`${environment.URL_API}/admin/tarifas`)
  }

  getTarifa(data){
    return this.http.post(`${environment.URL_API}/admin/tarifas`, data)
  }

  saveTarifa(data){
    return this.http.post(`${environment.URL_API}/admin/tarifa`, data)
  }

  updateTarifa(data){
    return this.http.put(`${environment.URL_API}/admin/tarifa`, data)
  }
}
