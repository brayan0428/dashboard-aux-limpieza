// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  URL_API: 'http://localhost/api-aux-limpieza/api',
  URL_FILES: 'http://localhost/api-aux-limpieza/images',
  KEY_AUTH_USER: '4uxL1mp13z4@2020'
};
